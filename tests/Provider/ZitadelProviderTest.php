<?php

namespace Tests\Provider;

use CHEZ14\OAuth2\Client\Provider\ZitadelProvider;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use PHPUnit\Framework\TestCase;
use League\OAuth2\Client\Token\AccessToken;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\TestWith;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

#[CoversClass(ZitadelProvider::class)]
class ZitadelProviderTest extends TestCase
{
    /** @var ZitadelProvider */
    protected $provider;

    protected function setUp(): void
    {
        $this->provider = new ZitadelProvider(['domain' => 'https://example.com']);
    }

    #[Test]
    #[TestDox('Constructor properly asks for domain input')]
    public function testConstructorRequiresDomain()
    {
        $this->expectExceptionMessage('domain is required');

        new ZitadelProvider([]);
    }

    #[Test]
    #[TestDox('Constructor properly set the domain')]
    #[TestWith(["https://example.com", "https://example.com"])]
    #[TestWith(["https://example.com/", "https://example.com"])]
    #[TestWith(["https://example.com/deep-link", "https://example.com/deep-link"])]
    #[TestWith(["https://example.com/deep-link/", "https://example.com/deep-link"])]
    #[Depends('testSetDomain')]
    public function testConstructorProperlySetDomain($test, $expectation)
    {
        $provider = new ZitadelProvider(['domain' => $test]);
        $this->assertEquals($expectation, $provider->getDomain());
    }

    #[Test]
    #[TestDox('Get base authorization URL returns correct value')]
    public function testGetBaseAuthorizationUrl()
    {
        $url = $this->provider->getBaseAuthorizationUrl();
        $this->assertEquals('https://example.com/oauth/v2/authorize', $url);
    }

    #[Test]
    #[TestDox('Get base access token URL returns correct value')]
    public function testGetBaseAccessTokenUrl()
    {
        $url = $this->provider->getBaseAccessTokenUrl([]);
        $this->assertEquals('https://example.com/oauth/v2/token', $url);
    }

    #[Test]
    #[TestDox('Get resource owner details URL returns correct value')]
    public function testGetResourceOwnerDetailsUrl()
    {
        $accessToken = $this->createMock(AccessToken::class);
        $url = $this->provider->getResourceOwnerDetailsUrl($accessToken);
        $this->assertEquals('https://example.com/oidc/v1/userinfo', $url);
    }

    #[Test]
    #[TestDox('Get default scopes returns expected scopes')]
    public function testGetDefaultScopes()
    {
        $method = new \ReflectionMethod(ZitadelProvider::class, 'getDefaultScopes');
        $method->setAccessible(true);
        $scopes = $method->invoke($this->provider);
        $this->assertContains('openid', $scopes);
        $this->assertContains('profile', $scopes);
    }

    #[Test]
    #[TestDox('Get scope separator returns expected separator')]
    public function testGetScopeSeparator()
    {
        $method = new \ReflectionMethod(ZitadelProvider::class, 'getScopeSeparator');
        $method->setAccessible(true);
        $separator = $method->invoke($this->provider);
        $this->assertEquals(' ', $separator);
    }

    #[Test]
    #[TestDox('Create Resource Owner retuns expected object')]
    public function testCreateResourceOwner()
    {
        $method = new \ReflectionMethod(ZitadelProvider::class, 'createResourceOwner');
        $method->setAccessible(true);
        $accessToken = $this->createMock(AccessToken::class);
        $resurceOwner = $method->invoke($this->provider, [
            'id' => 123,
            'email' => 'test@example.com',
            'name' => 'Test User',
            'login' => 'test_user',
        ], $accessToken);
        $this->assertNotNull($resurceOwner);
    }

    #[Test]
    #[TestDox('Check response throws exception only on error')]
    public function testCheckResponseThrowsExceptionOnError()
    {
        // Create a public method in the test class to call checkResponse
        $checkResponse = new \ReflectionMethod(ZitadelProvider::class, 'checkResponse');
        $checkResponse->setAccessible(true);

        // Mocking the ResponseInterface
        $response = $this->createMock(ResponseInterface::class);
        $stream = $this->createMock(StreamInterface::class); // Mock StreamInterface
        $response->method('getBody')->willReturn($stream); // Set the mocked StreamInterface as return value

        // Set up response for error scenario
        $response->method('getStatusCode')->willReturn(400);
        $responseObj = [
            'error_description' => 'Error description'
        ];
        $stream->method('getContents')->willReturn(json_encode($responseObj));

        // Expecting an IdentityProviderException
        $this->expectException(IdentityProviderException::class);
        $this->expectExceptionMessage('Error description');

        // Calling the public method to indirectly call checkResponse
        $checkResponse->invokeArgs($this->provider, [$response, $responseObj]);

        // Set up response for success scenario
        $response->method('getStatusCode')->willReturn(200);
        $stream->method('getContents')->willReturn('{"success": true}');

        // Now it should not throw an exception
        $checkResponse->invokeArgs($this->provider, [$response, []]);
    }

    #[Test]
    #[TestDox('Set domain properly truncate ending slash')]
    #[TestWith(["https://example.com", "https://example.com"])]
    #[TestWith(["https://example.com/", "https://example.com"])]
    #[TestWith(["https://example.com/deep-link", "https://example.com/deep-link"])]
    #[TestWith(["https://example.com/deep-link/", "https://example.com/deep-link"])]
    public function testSetDomain($test, $expectation)
    {
        $this->provider->setDomain($test);
        $this->assertEquals($expectation, $this->provider->getDomain());
    }

    #[Test]
    #[TestDox('Get User Console URL returns correct value')]
    public function testUetUserConsoleUrl()
    {
        $url = $this->provider->getUserConsoleUrl();
        $this->assertEquals('https://example.com/ui/console/users/me', $url);
    }

    #[Test]
    #[TestDox('Test setter and getter of PKCE Method')]
    public function testSetAndGetPkceMethod()
    {
        // Instantiate ZitadelProvider
        $provider = new ZitadelProvider(['domain' => 'example.com']);

        // Set PKCE Method
        $provider->setPkceMethod(ZitadelProvider::PKCE_METHOD_S256);

        // Get PKCE Method
        $method = new \ReflectionMethod(ZitadelProvider::class, 'getPkceMethod');
        $method->setAccessible(true);
        $pkceMethod = $method->invoke($provider);

        // Assert that PKCE Method is set correctly
        $this->assertEquals(ZitadelProvider::PKCE_METHOD_S256, $pkceMethod);
    }

    #[Test]
    #[TestDox('Get PKCE Method to be set as expected')]
    public function testGetPkceMethod()
    {
        $provider = new ZitadelProvider([
            'domain' => 'https://example.com',
        ]);
        $method = new \ReflectionMethod(ZitadelProvider::class, 'getPkceMethod');
        $method->setAccessible(true);

        // Make sure by default it sets to null
        $pkceMethod = $method->invoke($provider);
        $this->assertNull($pkceMethod);


        $provider->setPkceMethod(ZitadelProvider::PKCE_METHOD_S256);
        $pkceMethod = $method->invoke($provider);

        $url = $provider->getAuthorizationUrl([]);
        $codeChallenge = trim(
            strtr(
                base64_encode(hash('sha256', $provider->getPkceCode(), true)),
                '+/',
                '-_'
            ),
            '='
        );

        $this->assertEquals(ZitadelProvider::PKCE_METHOD_S256, $pkceMethod);
        $this->assertStringContainsString(urlencode($codeChallenge), $url);
    }
}
