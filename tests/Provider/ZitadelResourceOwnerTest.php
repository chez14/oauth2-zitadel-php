<?php

namespace Tests\Provider;

use PHPUnit\Framework\TestCase;
use CHEZ14\OAuth2\Client\Provider\ZitadelResourceOwner;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\Attributes\TestDox;

#[CoversClass(ZitadelResourceOwner::class)]
class ZitadelResourceOwnerTest extends TestCase
{
    protected $resourceOwner;

    protected function setUp(): void
    {
        $this->resourceOwner = new ZitadelResourceOwner([
            'id' => 123,
            'email' => 'test@example.com',
            'name' => 'Test User',
            'login' => 'test_user',
        ]);
        $this->resourceOwner->setDomain('https://example.com');
    }

    #[Test]
    #[TestDox('Get resource owner ID returns correct value')]
    public function testGetId()
    {
        $this->assertEquals(123, $this->resourceOwner->getId());
    }

    #[Test]
    #[TestDox('Get resource owner email returns correct value')]
    public function testGetEmail()
    {
        $this->assertEquals('test@example.com', $this->resourceOwner->getEmail());
    }

    #[Test]
    #[TestDox('Get resource owner name returns correct value')]
    public function testGetName()
    {
        $this->assertEquals('Test User', $this->resourceOwner->getName());
    }

    #[Test]
    #[TestDox('Get resource owner nickname returns correct value')]
    public function testGetNickname()
    {
        $this->assertEquals('test_user', $this->resourceOwner->getNickname());
    }

    #[Test]
    #[TestDox('Get resource owner URL returns correct value')]
    public function testGetUrl()
    {
        $this->assertEquals('https://example.com/test_user', $this->resourceOwner->getUrl());
    }

    #[Test]
    #[TestDox('To array converts resource owner details into array correctly')]
    public function testToArray()
    {
        $expected = [
            'id' => 123,
            'email' => 'test@example.com',
            'name' => 'Test User',
            'login' => 'test_user',
        ];
        $this->assertEquals($expected, $this->resourceOwner->toArray());
    }
}
