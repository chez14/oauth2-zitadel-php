<?php

namespace CHEZ14\OAuth2\Client\Provider;

use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Tool\ArrayAccessorTrait;
use League\OAuth2\Client\Tool\BearerAuthorizationTrait;
use Psr\Http\Message\ResponseInterface;

class ZitadelProvider extends AbstractProvider
{
    use ArrayAccessorTrait,
        BearerAuthorizationTrait;

    /**
     * Sets the domain of the Zitadel instance.
     *
     * @var string
     */
    protected string $domain;

    /**
     * Sets the PKCE Method.
     *
     * @var string|null
     */
    protected mixed $pkceMethod = null;

    /**
     * Constructs an OAuth 2.0 service provider.
     *
     * @param array $options An array of options to set on this provider.
     *     Options include `clientId`, `clientSecret`, `redirectUri`, `state`, and `domain`.
     * @param array $collaborators An array of collaborators that may be used to
     *     override this provider's default behavior. Collaborators include
     *     `grantFactory`, `requestFactory`, and `httpClient`.
     */
    function __construct(array $options = [], array $collaborators = [])
    {
        if (!key_exists('domain', $options)) {
            throw new \InvalidArgumentException('domain is required');
        }

        parent::__construct($options, $collaborators);
        $this->setDomain($options['domain']);
    }

    /**
     * Get authorization url to begin OAuth flow
     *
     * @return string
     */
    public function getBaseAuthorizationUrl()
    {
        return "{$this->domain}/oauth/v2/authorize";
    }

    /**
     * Get access token url to retrieve token
     *
     * @param array $params Query string parameters
     * @return string
     */
    public function getBaseAccessTokenUrl(array $params)
    {
        return "{$this->domain}/oauth/v2/token";
    }

    /**
     * Get provider url to fetch user details
     *
     * @param  AccessToken $token
     *
     * @return string
     */
    public function getResourceOwnerDetailsUrl(AccessToken $token)
    {
        return "{$this->domain}/oidc/v1/userinfo";
    }

    /**
     * Get the default scopes used by this provider.
     *
     * This should not be a complete list of all scopes, but the minimum
     * required for the provider user interface!
     *
     * @return array
     */
    protected function getDefaultScopes()
    {
        return ['openid', 'profile'];
    }

    /**
     * Returns the string that should be used to separate scopes when building
     * the URL for requesting an access token.
     *
     * @return string Scope separator, defaults to ' '
     */
    protected function getScopeSeparator()
    {
        return ' ';
    }

    /**
     * Check a provider response for errors.
     *
     * @throws IdentityProviderException
     * @param  ResponseInterface $response
     * @param  array|string $data Parsed response data
     * @return void
     */
    protected function checkResponse(ResponseInterface $response, $data)
    {
        $errors = [
            'error_description',
            'error.message',
        ];

        array_map(function ($error) use ($response, $data) {
            if ($message = $this->getValueByKey($data, $error)) {
                throw new IdentityProviderException($message, $response->getStatusCode(), $response);
            }
        }, $errors);
    }

    /**
     * Generate a user object from a successful user details request.
     *
     * @param  array $response
     * @param  AccessToken $token
     * @return ResourceOwnerInterface
     */
    protected function createResourceOwner(array $response, AccessToken $token)
    {
        return new ZitadelResourceOwner($response);
    }

    /**
     * Sets the domain of the Zitadel instance.
     *
     * @param string $domain
     * @return void
     */
    public function setDomain(string $domain): void
    {
        $this->domain = rtrim($domain, "/");
    }

    /**
     * Get the domain of the Zitadel instance.
     *
     * @return string
     */
    public function getDomain(): string
    {
        return $this->domain;
    }

    /**
     * Get URL to access user console.
     *
     * @return string
     */
    public function getUserConsoleUrl()
    {
        return "{$this->domain}/ui/console/users/me";
    }

    /**
     * Sets the PKCE Method.
     *
     * @param string|null $method The PKCE method to be set.
     * @return void
     */
    public function setPkceMethod($method): void
    {
        $this->pkceMethod = $method;
    }

    /**
     * Gets the PKCE Method.
     *
     * @return string|null The current PKCE method if set, null otherwise.
     */
    protected function getPkceMethod()
    {
        return $this->pkceMethod;
    }
}
